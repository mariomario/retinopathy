#!/bin/bash

cd $1

for nome in $(ls *.tif | sed s/'.tif'//g)
do
    convert "$nome".tif -resize 512x512\! "$nome".jpeg    
done

cd ..
mkdir -p jpegs_$1
mv $1/*.jpeg jpegs_$1 
