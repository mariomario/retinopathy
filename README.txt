Train a machine learning algorithm to classify rethinopaty grades
Images from http://www.adcis.net/en/Download-Third-Party/Messidor.htmlindex-en.php

zips folder: original zip files
Annotation_Basexx.csv: csv converted manually from xls with Excel on mac; only first line readable, certainly due to Excel bug or similar issue with newline
Basexx.csv: csv that works; converted from Annotation_Basexx.csv using cleanup.Rcalled by move.sh

HOW TO

unzip a zip file, e.g. Base22.zip
put the .tif files in a folder Base22
this step already done (folders in unused_for_now)
run ./squeeze.sh Base22 (or whatever the name is)
it will create jpegs_Base22 with 512x512 jpeg files
create (if not there already) 0 1 2 3 folders
make sure you have Annotation_Base22.csv in the main folder, where you are working
run ./move.sh Base22
it should produce a Base22.csv properly formatted, and put the jpegs from jpegs_Base22 in the proper 0, 1, 2, 3 folders based on their level or retinopathy

in folder learn we do the real learning
For now I put all 1, 2, 3 together in host_images and 0 in nonhost_images
I randomly split in train and test, keeping half and half
then after the split I flipflopped the images in train/host_images, train/nonhost_images, test/ etc...
This way we train on images that are rigorously different from the testing ones


