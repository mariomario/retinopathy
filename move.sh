#!/bin/bash

cat cleanup.R | sed s/"SUBSTITUTEMEWITHDOLLAR1"/$1/g > tmp.R
rm .R*
R CMD BATCH tmp.R
cat tmp.Rout
rm tmp.Rout
mv base.tmp $1.csv
for nome in $(cat $1.csv | cut -d "," -f 1,3 | sed s/"tif"/"jpeg"/g)
do
    cp jpegs_$1/$(echo $nome | sed s/","/" "/g)
done
