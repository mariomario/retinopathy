#!/bin/bash

targetfolder=$(echo $1 | sed s/'\/$'//g)

for nome in $(ls $targetfolder);
do
    convert -flip "$targetfolder"/"$nome" "$targetfolder"/flip_"$nome"
done

for nome in $(ls $targetfolder);
do
    convert -flop "$targetfolder"/"$nome" "$targetfolder"/flop_"$nome"
done
