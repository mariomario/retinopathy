from keras.models import load_model
import matplotlib.image as mpimg
import numpy as np
import os

model = load_model('model.hd5')

def imgprob(filename):
    img = mpimg.imread(filename)
    img.shape = [1,512,512,3] #need to do this to feed it into the model
    img = img.astype('float32')
    img = img/255 #also this, to go from integers to [0,1]
    p = model.predict(img)
    return p

def predictimg(filename):
    p = imgprob(filename)
    predicted_class_y = np.argmax(p)
    return predicted_class_y

def list_test_files(folder):
    test_files = []
    for dirname, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            test_files.append(os.path.join(dirname, filename))
    return(test_files)

legenda = ["healthy", "retinopathy"]
cerchiati = 0
noncerchiati = 0 

print "trueClass predictedClass pRetinopathy"

for test_file in list_test_files('test/host_images'):
    predicted = predictimg(test_file)
    noncerchiati += predicted
    cerchiati += (1 - predicted)
    print "retinopathy" + " " + legenda[predicted] + " " + str(imgprob(test_file)[0][1])
for test_file in list_test_files('test/nonhost_images'):
    predicted = predictimg(test_file)
    noncerchiati += predicted
    cerchiati += (1 - predicted)
    print "healthy" + " " + legenda[predicted] + " " + str(imgprob(test_file)[0][1])


#print "Predicted " + legenda[0] + "s " + str(cerchiati)
#print "Predicted " + legenda[1] + "s " + str(noncerchiati)
